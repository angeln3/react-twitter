const admin = require('firebase-admin');

//entramos a la base como admin
admin.initializeApp();

const db = admin.firestore();

module.exports = {admin, db};