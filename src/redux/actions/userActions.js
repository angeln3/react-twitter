import {SET_USER, SET_ERRORS, CLEAR_ERRORS, LOADING_UI} from '../types';
import axios from 'axios';

export const loginUser = (userData, history) => (dispatch) => {
    //loading = true
    dispatch({type: LOADING_UI});
     //enviamos el email y la contraseña
     axios
     .post("/login", userData)
     .then((res) => {
       const FBIdToken = `Bearer ${res.data.token}`;
       //por si refresca la pagina guardarlo en el local storage
       localStorage.setItem("FBIdToken", FBIdToken);
       axios.defaults.headers.common['Authorization'] = FBIdToken;
       dispatch(getUserData());
       dispatch({ type: CLEAR_ERRORS});
       //lo enviamos al home
       history.push("/");
     })
     .catch((err) => {
       //asignamos los errores al state
       dispatch({
           type: SET_ERRORS,
           payload: err.response.data
       })
     });
}

export const getUserData = () => (dispatch) =>{
    axios.get('/user')
        .then(res =>{
            dispatch({
                type: SET_USER,
                payload: res.data
            })
        })
        .catch(err =>{
            console.log(err);
        })
}