import React from "react";
//router para cambiar en una sola pagina
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
//-REDUX-
import { Provider } from "react-redux";
import store from "./redux/store";
//paleta de colores MUI
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import themeFile from "./utils/theme";
//Components
import NavBar from "./components/Navbar";
//pages
import home from "./pages/home";
import login from "./pages/login";
import signup from "./pages/signup";
import apiUrl from "./pages/apiUrl";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
//Autentificacion de rutas
import AuthRoute from "./utils/AuthRoute";
//js-decode
import jwtDecode from "jwt-decode";

//paleta colors
const theme = createMuiTheme(themeFile);

const token = localStorage.FBIdToken;

let autenticated;
if (token) {
  const decodedToken = jwtDecode(token);
  //para ver si expiro el token
  if (decodedToken.exp * 1000 < Date.now()) {
    window.location.href = "/login";
    autenticated = false;
  } else {
    autenticated = true;
  }
}

class App extends React.Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <Provider store={store}>
          <div className="App">
            <Router>
              <div className="container">
                <NavBar />
                <Switch>
                  <Route exact path="/" component={home} />
                  <AuthRoute
                    exact
                    path="/login"
                    component={login}
                    autenticated={autenticated}
                  />
                  <AuthRoute
                    exact
                    path="/signup"
                    component={signup}
                    autenticated={autenticated}
                  />
                  <Route exact path="/apiUrl" component={apiUrl} />
                </Switch>
              </div>
            </Router>
          </div>
        </Provider>
      </MuiThemeProvider>
    );
  }
}

export default App;
