import React from 'react';
//usar links(a) con Router - npm install react-router-dom
import {Link} from 'react-router-dom';
//materia-UI - npm install @material-ui/core
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';

class Navbar extends React.Component{

  render(){
    return (
        <AppBar>
          <Toolbar className="nav-container">
            <Button color="inherit" component={Link} to="/login" >Login</Button>
            <Button color="inherit" component={Link} to="/" >Home</Button>
            <Button color="inherit" component={Link} to="/signup" >Signup</Button>
            <Button color="inherit" component={Link} to="/apiUrl" >apiUrl</Button>
          </Toolbar>
        </AppBar>
    );
  }

}

export default Navbar;
