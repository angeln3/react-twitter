import React from 'react';

class Form extends React.Component {

  state = {
    texto: "",
    check: false,
    check1: false,
    check2: false
  }

  handleCaptura = (e) => {
    const { value, name } = e.target;
    this.setState({
      [name]: value
    })
  }
  handleCheck = (e) => {
    const { checked, name } = e.target;
    this.setState({
      [name]: checked
    })
  }

  handleButton = () => {
    this.props.addTexto(this.state)
    console.log(this.state);

  }
  render() {
    return (

      <section>
        <input type="checkbox" onChange={this.handleCheck} name="check" ></input>
        <input type="text" onChange={this.handleCaptura} name="texto"></input>
        <button onClick={this.handleButton}>imprimir</button>
        <br />

        <input type="checkbox" onClick={this.handleCheck} name="check1"></input>
        <input type="text" onChange={this.handleCaptura} name="texto1"></input>
        <br />
        <input type="checkbox" onClick={this.handleCheck} name="check2"></input>
        <input type="text" onChange={this.handleCaptura} name="texto2"></input>
      </section>
    );


  }

}

export default Form;