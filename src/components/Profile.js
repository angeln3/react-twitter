import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import User_img from '../img/user.jpg';
//icons
import LocationOn from '@material-ui/icons/LocationOn'
import LinkIcon from '@material-ui/icons/Link'
import CalendarToday from '@material-ui/icons/CalendarToday'
import { Link } from '@material-ui/core';

const styles ={
    card:{
        display: 'flex'
    },
    img: {
        width: '150px',
        margin: '20px',
       "border-radius": '100px'
      }
}

class Profile extends React.Component{

  render(){
    const {classes} = this.props;
    return (
        <Paper >
            <div>
             <img src={User_img} className={classes.img}/>
            </div>
            <div>
                <LocationOn color="primary"/><span>Mi dirección</span>       
            </div>
            <div>
                <LinkIcon color="primary"/><span>Link</span>   
            </div>
            <div>
                <CalendarToday color="primary"/><span>Calendario</span>
            </div> 
           
        </Paper>
   ); 
  }
   
} 

export default withStyles(styles)(Profile);
