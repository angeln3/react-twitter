import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
//MUI stuff
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
//route
import {Link} from 'react-router-dom';
//daaay js para ver la fecha en que se creo el scream
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';

const styles ={
    card:{
        display: 'flex',
        marginBottom: 20
    },
    image:{
        minWidth: 200,
        objetFit: 'cover'
    },
    content:{
        padding: 25
    }
}

class Scream extends React.Component{

  render(){

      dayjs.extend(relativeTime)

      const {
          classes,
          data:{
              body,
              createdAt,
              userImage,
              userHandle,
              screamId,
              likeCount,
              commentCount
  
          }
      } = this.props;
    return (
        <Card className={classes.card}>
            <CardMedia title="profile image" image={userImage} className={classes.image}/>
            <CardContent className={classes.content}>
                <Typography variant="h5" color="primary" component={Link} to={`/users/${userHandle}`}>{userHandle}</Typography>
                <Typography variant="body2" color="textSecundary">{dayjs(createdAt).fromNow()}</Typography>
                <Typography variant="body1">{body}</Typography>  
                <Typography>{}</Typography>  
            </CardContent> 
        </Card>
   ); 
  }
   
} 

export default withStyles(styles)(Scream);
