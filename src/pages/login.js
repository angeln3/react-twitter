import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import PropTypes from "prop-types";
import Icon from "../img/mcq.png";
//MUI Stuff
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
//axios
import axios from "axios";
//link
import { Link } from "react-router-dom";

//Redux Stuff
import {connect} from 'react-redux';
import {loginUser} from '../redux/actions/userActions';

const styles = {

  form: {
    textAling: "center",
  },
  img: {
    width: "50px",
    margin: "20px",
  },
  items: {
    marginBottom: 20,
  },
  button: {
    marginTop: 20,
    position: "relative",
  },
  customError: {
    color: "red",
    fontSize: "0.8rem",
    marginTop: 10,
  },
  progress: {
    position: "absolute",
  }

}

class signup extends React.Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      errors: {},
    };
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.UI.errors){
      this.setState({errors: nextProps.UI.errors});
    }
  }

  Submit = (e) => {
    e.preventDefault();
    const userData = {
      email: this.state.email,
      password: this.state.password,
    };
    //aqui llamamos a la funcion que creamos en redux
    this.props.loginUser(userData, this.props.history);
   
  };

  handleChange = (e) => {
    //obtenemos el name y valor de cada input
    const { name, value } = e.target;

    this.setState({
      [name]: value,
    });
  };

  render() {
    const { classes, UI: {loading} } = this.props;
    const { email, password, errors } = this.state;
    return (
      <Grid container className={classes.form}>
        <Grid item sm />
        <Grid item sm>
          <img src={Icon} className={classes.img} />
          <Typography variant="h4">Login</Typography>
          <form onSubmit={this.Submit}>
            <TextField
              id="email"
              value={email}
              onChange={this.handleChange}
              helperText={errors.email}
              error={errors.email ? true : false}
              label="email"
              name="email"
              type="email"
              fullWidth
              className={classes.items}
            />
            <TextField
              id="password"
              value={password}
              onChange={this.handleChange}
              helperText={errors.password}
              error={errors.password ? true : false}
              label="password"
              name="password"
              type="password"
              fullWidth
              className={classes.items}
            />
            {/*por si hay algun otro error*/}
            {errors.general && (
              <Typography variant="body2" className={classes.customError}>
                {errors.general}
              </Typography>
            )}
            <Button
              className={classes.button}
              type="submit"
              color="primary"
              variant="contain"
              onClick={this.Submit}
              disabled={loading}
            >
              Enviar
              {loading && (
                <CircularProgress className={classes.progress} size={30} />
              )}
            </Button>
            <br />
            <small>
              No tienes una cuenta? <Link to="/signup">registrate</Link>
            </small>
          </form>
        </Grid>
        <Grid item sm />
      </Grid>
    );
  }
}

signup.propTypes = {
  classes: PropTypes.object.isRequired,
  loginUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  UI: PropTypes.object.isRequired
};

const mapStateToProps = (state) =>({
  user: state.user,
  UI: state.UI
});

const mapActionsToProps = {
  loginUser
}


export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles)(signup));
