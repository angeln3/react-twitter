import React from 'react';

//import { stores } from './tiendas';

//console.log(stores);

class ApiUrl extends React.Component{

    constructor(){
        super();

        this.state = {
            data: [],
            data2: [],
            isFetch: true,
        }

    }
    componentDidMount(){
        fetch('https://jsonplaceholder.typicode.com/users')
        //promise
        .then(res => res.json())
        .then(datos => this.setState({data: datos}));

        fetch('https://api.myjson.com/bins/9ucfi')
        //promise
        .then(res => res.json())
        //importante revisar los datos del servicio
        .then(datos => this.setState({data2: datos.data, isFetch: false}));
    }

  render(){

    //para controlar la promesa y no me de undefinido cuando aun no carga
    if (this.state.isFetch){
        return 'loading...'
    }

      //recorrer el json con map
      var usuarios = this.state.data.map( (datos,i) => {

          return <p key={i}>{datos.name}, {datos.email}</p>                

      });

      //accedemos a content desde aqui y no desde la promesa por si queremos acceder a otro datos
        var tiendas = this.state.data2.content.map( (datos,i) => {

            return <p key={i}>{datos.state}</p>                

        });

       var nombreT = this.state.data2.content.map( (datos,i) => {
             const tiendas = Object.values(datos.stores);
             return (
                 tiendas.map((tindas,i)=>{
               return (
                    <p key={i}>{tindas.name}</p>
                )            
               })
            )          

        });


    return (
        <div>
            <div>
                <h1>ApiUrl Usuarios</h1>
                <ul>{usuarios}</ul>
            </div>
            <div>
                <h1>Tinedas</h1>
                <table>
                    <tr>
                        <th>tindas</th>
                        <th>nombre de tinda</th>
                    </tr>
                    <tr>
                        <td>{tiendas}</td>
                        <td>{nombreT}</td>
                    </tr>
                </table>
            </div>
        </div>
    );
  }

}

export default ApiUrl;