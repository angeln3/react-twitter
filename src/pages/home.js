import React from 'react';
import { Grid } from '@material-ui/core';
import Scream from '../components/Scream';
import Profile from '../components/Profile';
import axios from 'axios';


//la rejilla es de 12, asi que tenemos que completar los bloques
class home extends React.Component{

  constructor(){
    super();

    this.state = {
        screams: null
    }

}
componentDidMount(){
   axios.get("/screams")
   .then(res =>{
     this.setState({
       screams: res.data
     })
   })
   .catch(err => console.log(err));
}

  render(){

    const {screams} = this.state;

    let resentScreams = screams ? (
      screams.map( (datos) => {
        return <Scream key={datos.screamId} data={datos} />  
      }) ) : (
      <p>Loading...</p>
      );
                  


    return (
        <Grid container spacing={1}>

          <Grid item sm={8} xs={12}>
            {resentScreams}
          </Grid>

          <Grid item sm={4} xs={12}>
            <Profile />
          </Grid>

        </Grid>
    );
  }

}

export default home;
