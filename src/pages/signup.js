import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import PropTypes from "prop-types";
import Icon from "../img/mcq.png";
//MUI Stuff
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
//axios
import axios from "axios";
//link
import { Link } from "react-router-dom";

const styles = {

  form: {
    textAling: "center",
  },
  img: {
    width: "50px",
    margin: "20px",
  },
  items: {
    marginBottom: 20,
  },
  button: {
    marginTop: 20,
    position: "relative",
  },
  customError: {
    color: "red",
    fontSize: "0.8rem",
    marginTop: 10,
  },
  progress: {
    position: "absolute",
  }

}

class signup extends React.Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      confirmPassword: '',
      handle: '',
      loading: false,
      errors: {},
    };
  }

  Submit = (e) => {
    e.preventDefault();
    this.setState({
      loading: true,
    });
    const NewUserData = {
      email: this.state.email,
      password: this.state.password,
      confirmPassword: this.state.confirmPassword,
      handle: this.state.handle
    };
    console.log(NewUserData);
    //enviamos el email y la contraseña
    axios
      .post("/signup", NewUserData)
      .then((res) => {
        console.log(res.data);
        //por si refresca la pagina guardarlo en el local storage
        localStorage.setItem("FBIdToken", `Bearer ${res.data.token}`);
        this.setState({
          loading: false,
        });
        //lo enviamos al home
        this.props.history.push("/");
      })
      .catch((err) => {
        //asignamos los errores al state
        this.setState({
          errors: err.response.data,
          loading: false,
        });
      });
  };

  handleChange = (e) => {
    //obtenemos el name y valor de cada input
    const { name, value } = e.target;

    this.setState({
      [name]: value,
    });
  };

  render() {
    const { classes } = this.props;
    const { email, password, loading, errors, confirmPassword,
    handle } = this.state;
    return (
      <Grid container className={classes.form}>
        <Grid item sm />
        <Grid item sm>
          <img src={Icon} className={classes.img} />
          <Typography variant="h4">Signup</Typography>
          <form onSubmit={this.Submit}>
            <TextField
              id="email"
              value={email}
              onChange={this.handleChange}
              helperText={errors.email}
              error={errors.email ? true : false}
              label="email"
              name="email"
              type="email"
              fullWidth
              className={classes.items}
            />
            <TextField
              id="password"
              value={password}
              onChange={this.handleChange}
              helperText={errors.password}
              error={errors.password ? true : false}
              label="password"
              name="password"
              type="password"
              fullWidth
              className={classes.items}
            />
             <TextField
              id="confirmPassword"
              value={confirmPassword}
              onChange={this.handleChange}
              helperText={errors.confirmPassword}
              error={errors.confirmPassword ? true : false}
              label="confirmPassword"
              name="confirmPassword"
              type="password"
              fullWidth
              className={classes.items}
            />
             <TextField
              id="handle"
              value={handle}
              onChange={this.handleChange}
              helperText={errors.handle}
              error={errors.handle ? true : false}
              label="Handle"
              name="handle"
              type="text"
              fullWidth
              className={classes.items}
            />
            {/*por si hay algun otro error*/}
            {errors.general && (
              <Typography variant="body2" className={classes.customError}>
                {errors.general}
              </Typography>
            )}
            <Button
              className={classes.button}
              type="submit"
              color="primary"
              variant="contain"
              onClick={this.Submit}
              disabled={loading}
            >
              Signup
              {loading && (
                <CircularProgress className={classes.progress} size={30} />
              )}
            </Button>
            <br />
            <small>
              Ya tienes una cuenta? <Link to="/login">ingresa</Link>
            </small>
          </form>
        </Grid>
        <Grid item sm />
      </Grid>
    );
  }
}

signup.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(signup);