import React from 'react';
import logo from './logo.svg';
import './App.css';
import Form from './components/form';

class App2 extends React.Component{

  state = {
    texto: "",
    check: ''
  }
  handleAgregarInfo = (informacion) =>{
    const info = informacion;
    for (var inf in info) {

    this.setState({
      [inf] : info[inf]
    })
  }
  }
  render(){
    return (
      <div className="App2">
        <header className="App2-header">
          <img src={logo} className="App2-logo" alt="logo" />
          <p>
            Edit <code>src/App2.js</code> and save to reload.
          </p>
          <a
            className="App2-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
        <Form addTexto={this.handleAgregarInfo}/>
        
        <p><b>{this.state.check.toString()}-{this.state.texto}</b></p>
        <br/>
        <p><b>{this.state.check1}-{this.state.texto1}</b></p>
        <br/>
        <p><b>{this.state.check2}-{this.state.texto2}</b></p>

      </div>
    );


  }

}

export default App2;
