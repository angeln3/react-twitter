import React from 'react'
import { Route, Redirect } from 'react-router-dom';

const AuthRoute = ({component: Component, autenticated, ...res}) => (
    <Route
    {...res}
    render={(props)=> autenticated === true ? <Redirect to='/'/> : <Component {...props} />}
    />
);

export default AuthRoute;