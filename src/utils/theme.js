export default {
    palette: {
        primary: {
          light: '#ff784e',
          main: '#ff5722',
          dark: '#b23c17',
          contrastText: '#fff'
        },
        secondary: {
          light: '#ffcf33',
          main: '#ffc400',
          dark: '#b28900',
          contrastText: '#fff'
        }
      },
      form: {
        textAling: "center",
      },
      img: {
        width: "50px",
        margin: "20px",
      },
      items: {
        marginBottom: 20,
      },
      button: {
        marginTop: 20,
        position: "relative",
      },
      customError: {
        color: "red",
        fontSize: "0.8rem",
        marginTop: 10,
      },
      progress: {
        position: "absolute",
      }
}